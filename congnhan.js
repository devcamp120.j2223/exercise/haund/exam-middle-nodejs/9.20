
import { Person } from "./person.js";

class CongNhan extends Person{
    constructor(paramFullName, paramBirth, paramAdress, paramJob, paramJobSite, paramSalary){
        super(paramFullName, paramBirth, paramAdress)
        this.job = paramJob;
        this.jobSite = paramJobSite;
        this.salary = paramSalary;
    }
    callClassCongNhan(){
        console.log("CongNhan class is calling");
        return this
    }
}

export { CongNhan }