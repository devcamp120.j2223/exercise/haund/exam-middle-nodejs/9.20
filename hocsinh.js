
import { Person } from "./person.js";

class HocSinh extends Person{
    constructor(paramFullName, paramBirth, paramAdress, paramSchool, paramClass, paramPhone){
        super(paramFullName, paramBirth, paramAdress)

            this.school = paramSchool;
            this.class = paramClass;
            this.phone = paramPhone;
        
    }
    callClassHocSinh(){
        console.log("HocSinh class is calling");
        return this;
    }
}

export { HocSinh }