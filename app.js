
import { Person } from "./person.js";
import { HocSinh } from "./hocsinh.js";
import { SinhVien } from "./sinhvien.js";
import { CongNhan } from "./congnhan.js";


let person = new Person("Nguyễn Bảo Khánh", 19960403, "Bến Tre");
console.log(person.fullName);
console.log(person.birth);
console.log(person.address);
console.log(person.callPerson());


let hocsinh = new HocSinh("Nguyễn Bảo Khánh" , 19960403, "Bến Tre", "THPT CheGueVara", "12A2", 918130023);

console.log(hocsinh.callClassHocSinh());

let sinhvien = new SinhVien("Nguyễn Bảo Khánh" , 19960403, "Bến Tre", "ĐH Cần Thơ", "CN14V602", 918130023, "Công nghệ hoá học", "B1407660");
console.log(sinhvien.callClassSinhVien());

let congnhan = new CongNhan("Nguyễn Bảo Khánh" , 19960403, "Bến Tre", "Bán vé số", "Khắp mọi nơi", 1000000000);
console.log(congnhan.callClassCongNhan());