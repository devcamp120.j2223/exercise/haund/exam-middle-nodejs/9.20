

class Person{
    constructor(paramFullName, paramBirth, paramAdress){
        this.fullName = paramFullName;
        this.birth = paramBirth;
        this.address = paramAdress;
    }
    callPerson(){
        console.log("Person class is calling");
        return this
    }
}

export { Person }