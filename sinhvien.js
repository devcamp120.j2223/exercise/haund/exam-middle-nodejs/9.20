
import { HocSinh } from "./hocsinh.js";

class SinhVien extends HocSinh{
    constructor(paramFullName, paramBirth, paramAdress, paramSchool, paramClass, paramPhone, paramMajor, paramStudentCode){
        super(paramFullName, paramBirth, paramAdress, paramSchool, paramClass, paramPhone)
        this.major = paramMajor;
        this.code = paramStudentCode
    }
    callClassSinhVien(){
        console.log("SinhVien class is calling");
        return this;
    }
}

export {SinhVien}